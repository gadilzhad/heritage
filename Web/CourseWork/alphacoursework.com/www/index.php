<?php

define('INCLUDE_CHECK',true);

require 'connect.php';
require 'functions.php';

session_name('UILogin');
session_set_cookie_params(2*7*24*60*60);
session_start();

if($_SESSION['id'] && !isset($_COOKIE['uiRemember']) && !$_SESSION['rememberMe'])
{
	$_SESSION = array();
	session_destroy();

}

if(isset($_GET['logoff']))
{
	$_SESSION = array();
	session_destroy();
	
	header("Location: index.php");
	exit;
}

if($_POST['submit']=='Login')
{
	
	$err = array();
	
	if(!$_POST['username'] || !$_POST['password'])
		$err[] = 'All the fields must be filled in!';
	
	if(!count($err))
	{
		$_POST['username'] = mysql_real_escape_string($_POST['username']);
		$_POST['password'] = mysql_real_escape_string($_POST['password']);
		$_POST['rememberMe'] = (int)$_POST['rememberMe'];
		
		// Escaping all input data

		$row = mysql_fetch_assoc(mysql_query("SELECT id,usr FROM ui_members WHERE usr='{$_POST['username']}' AND pass='".md5($_POST['password'])."'"));

		if($row['usr'])
		{
			
			$_SESSION['usr']=$row['usr'];
			$_SESSION['id'] = $row['id'];
			$_SESSION['rememberMe'] = $_POST['rememberMe'];
			
			
			setcookie('uiRemember',$_POST['rememberMe']);
		}
		else $err[]='Wrong username and/or password!';
	}
	
	if($err)
	$_SESSION['msg']['login-err'] = implode('<br />',$err);

	header("Location: index.php");
	exit;
}
else if($_POST['submit']=='Register')
{
	$err = array();
	
	if(strlen($_POST['username'])<4 || strlen($_POST['username'])>32)
	{
		$err[]='Your username must be between 3 and 32 characters!';
	}
	
	if(preg_match('/[^a-z0-9\-\_\.]+/i',$_POST['username']))
	{
		$err[]='Your username contains invalid characters!';
	}
	
	if(!checkEmail($_POST['email']))
	{
		$err[]='Your email is not valid!';
	}
	
	if(!count($err))
	{
		// Довольно рандомные пароли получаются, как по мне:
		$pass = substr(md5($_SERVER['REMOTE_ADDR'].microtime().rand(1,100000)),0,6);
		
		$_POST['email'] = mysql_real_escape_string($_POST['email']);
		$_POST['username'] = mysql_real_escape_string($_POST['username']);
		
		$usr = $_POST['username'];
		$password = md5($pass);
		$usr_email = $_POST['email'];
		
		mysql_query("	INSERT INTO ui_members(id,usr,pass,email)
						VALUES('NULL', '$usr', '$password', '$usr_email')");
		
		if(mysql_affected_rows($link)==1)
		{
			send_mail(	'admin@alphacoursework.com',
						$_POST['email'],
						'SystemInfo password',
						'Your password is: '.$pass);

			$_SESSION['msg']['reg-success']='We have sent you an email with your new password!';
		}
		else $err[]='This username is already taken!';
	}

	if(count($err))
	{
		$_SESSION['msg']['reg-err'] = implode('<br />',$err);
	}	
	
	header("Location: index.php");
	exit;
}

$script = '';

if($_SESSION['msg'])
{
	
	$script = '
	<script type="text/javascript">
	
		$(function(){
		
			$("div#panel").show();
			$("#toggle a").toggle();
		});
	
	</script>';
	
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SystemInfo</title>

    <link rel="stylesheet" type="text/css" href="login_panel/css/slide.css" media="screen" />  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script src="login_panel/js/slide.js" type="text/javascript"></script>  
    <?php echo $script; ?>
	<link type="text/css" rel="stylesheet" href="css/style.css">

</head>

<body>

<div id="toppanel">
	<div id="panel">
		<div class="content clearfix">
			<div class="left">
			</div>
            
            
            <?php
			
			if(!$_SESSION['id']):
			
			?>
            
			<div class="left">
				<form class="clearfix" action="" method="post">
					<h1>Member Login</h1>
                    
                    <?php
						
						if($_SESSION['msg']['login-err'])
						{
							echo '<div class="err">'.$_SESSION['msg']['login-err'].'</div>';
							unset($_SESSION['msg']['login-err']);
						}
					?>
					
					<label class="grey" for="username">Username:</label>
					<input class="field" type="text" name="username" id="username" value="" size="23" />
					<label class="grey" for="password">Password:</label>
					<input class="field" type="password" name="password" id="password" size="23" />
	            	<label><input name="rememberMe" id="rememberMe" type="checkbox" checked="checked" value="1" /> &nbsp;Remember me</label>
        			<div class="clear"></div>
					<input type="submit" name="submit" value="Login" class="bt_login" />
				</form>
			</div>
			<div class="left right">			
				<form action="" method="post" accept-charset="UTF-8">
					<h1>Not a member yet? Sign Up!</h1>		
                    
                    <?php
						
						if($_SESSION['msg']['reg-err'])
						{
							echo '<div class="err">'.$_SESSION['msg']['reg-err'].'</div>';
							unset($_SESSION['msg']['reg-err']);
						}
						
						if($_SESSION['msg']['reg-success'])
						{
							echo '<div class="success">'.$_SESSION['msg']['reg-success'].'</div>';
							unset($_SESSION['msg']['reg-success']);
						}
					?>
                    		
					<label class="grey" for="username">Username:</label>
					<input class="field" type="text" name="username" id="username" value="" size="23" />
					<label class="grey" for="email">Email:</label>
					<input class="field" type="text" name="email" id="email" size="23" />
					<label>A password will be e-mailed to you.</label>
					<input type="submit" name="submit" value="Register" class="bt_register" />
				</form>
			</div>
            
            <?php
			
			else:
			
			?>
            
            <div class="left">
            
            <h1>SystemInfo:</h1>
            
            <p>Click on the button</p>
			<a href="report.php" class="info">Get Info!</a>
			<br \>
			<p> or you can log off: </p>
			
			<a href="?logoff">Log off</a>
            </div>
            
			<div class="left">
			<h1>Author:</h1>
			<p>G.D. Arshakian <br \></p>
			<p>Student of 2nd course <br \></p>
			<p>FI-21, IPT, NTUU "KPI"<br \></p>
			</div>
			
            
            </div>
            
            <?php
			endif;
			?>
		</div>
	</div>	

	<div class="tab">
		<ul class="login">
	    	<li class="left">&nbsp;</li>
	        <li>Hello <?php echo $_SESSION['usr'] ? $_SESSION['usr'] : 'Guest';?>!</li>
			<li class="sep">|</li>
			<li id="toggle">
				<a id="open" class="open" href="#"><?php echo $_SESSION['id']?'Information Here':'Log In | Register';?></a>
				<a id="close" style="display: none;" class="close" href="#">Close Panel</a>			
			</li>
	    	<li class="right">&nbsp;</li>
		</ul> 
	</div> 
	
</div>

<div id="wrap">
  <div id="header">
    <h1><a href="index.php">SystemInfo</a></h1>
    <h2>Course Work of Arshakian G.D.</h2>
    
  </div>
  <div id="frontpage-content">
    <div id="frontpage-intro">
      <p>At <span>SystemInfo</span> you can easily download <span>info about your PC</span>. All you have to do is <span>to register</span>,  and then you'll see a button to view <span> a report.</span></p>
    </div>
  
  
  </div>
   <div class="left">
            
  <div id="footer">
    <p class="copyright">Developed by Arshakian G.D. for educational purpose</p>
  </div>
</div>

</body>
</html>
