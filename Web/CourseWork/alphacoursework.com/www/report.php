

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SystemInfo: Report</title>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
    <script type="text/javascript">	
		function getInfo(){
		var JSPlatform = navigator.platform;
		var JSWidth = screen.width;
		var JSHeight = screen.height;
		var JSBrowserName= navigator.appCodeName;
		var JSJava = navigator.javaEnabled();
		var JSLanguage = navigator.language;
		var JSCookie = navigator.cookieEnabled;
		$(document).ready(function() {
		$.post(
		'reporter.php', 	
		{
		platform: JSPlatform,
		width: JSWidth,
		height: JSHeight,
		browserName: JSBrowserName,
		java: JSJava,
		language: JSLanguage,
		cookie: JSCookie},
		function(data){
			$('#report').append(data);
		});
		});
		}
	</script>
</head>

<body onLoad="getInfo();">

	<div id="main">
		<div id="wrap">
			<div id="header">
			<h1><a href="index.php">SystemInfo</a></h1>
					<h2>Report:</h2>
  </div>
    <div>
	
	<div id="main">
		<div id="content">
			<div id="report"></div>
    </div>
				
    </div>	
		
    </div>
    
 
</body>
</html>
