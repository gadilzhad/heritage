package com.arshakyan.lab1;

/**
 *
 * @author Jerzy Arshakyan
 */
public class IOExceptionWrapper extends Exception {

    private String msg;

    IOExceptionWrapper() {
        msg = "Default message";
    }

    IOExceptionWrapper(String s) {
        msg = s;
    }

    @Override
    public String toString() {
        return "IOExceptionWrapper: (" + msg + ")";  
}  
}
