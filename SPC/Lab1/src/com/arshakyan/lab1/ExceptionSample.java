package com.arshakyan.lab1;

import java.io.*;
/**
 *
 * @author Jerzy Arshakyan
 */
public class ExceptionSample {

    public static void main(String[] args) throws IOExceptionWrapper  {
        exceptionGenerator();
    }

    private static void exceptionGenerator() throws IOExceptionWrapper  {
        generateNullPointerException();
        generateIOException();
    }

    private static void generateNullPointerException() {
        try {
            throw new NullPointerException("Example of null pointer error");
            /* Same as, for example:
             * null.toString();
             */
        } catch (NullPointerException exception) {
            System.out.println(exception.toString());
        }
    }

    private static void generateIOException() throws IOExceptionWrapper  {
        try {
            throw new IOException("Example of input/output error");
            /* Same errors be caused 
             * when file cannot be read, or other I/O operations
             */
        } catch (IOException exception) {
            IOExceptionWrapper iowrap = new IOExceptionWrapper();
            iowrap.initCause(exception);
            throw iowrap;
        }
    }
}
