/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arshakyan.lab5;

import java.util.Vector;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ezalor
 */
public class ScalarRunTest {

    
    
    public ScalarRunTest() {
        
    }

    
    @Test
    public void testMultiplication() {
        VectorWrapper firstVector = new VectorWrapper();
        VectorWrapper secondVector = new VectorWrapper();
        firstVector.initVector("firstVector.txt");
        secondVector.initVector("secondVector.txt");
        
        
        ScalarMultiplicator scalar = new ScalarMultiplicator(firstVector, secondVector);
        scalar.multiple();
        assertEquals(scalar.getResult(), (Double) 3.0);
    }
    
  /* 
        @Test 
   public void testParallelMultiplication() {
        VectorWrapper firstVector = new VectorWrapper();
        VectorWrapper secondVector = new VectorWrapper();
        firstVector.initVector("firstVector.txt");
        secondVector.initVector("secondVector.txt");
        
        
        ScalarMultiplicator scalar = new ScalarMultiplicator(firstVector, secondVector);
        //scalar.CheckedParallelMultiplicator();
        assertEquals(scalar.getResult(), (Double) 3.0);
    }
    
    */
    
    @Test
    public void testVectorWrapper(){
        VectorWrapper firstVector = new VectorWrapper();
        firstVector.initVector("firstVector.txt");
        
       assertEquals(3, firstVector.size());
    }
}