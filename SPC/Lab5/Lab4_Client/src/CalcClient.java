import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 *
 * @author Jerzy Arshakyan
 */
public class CalcClient {
    
    private static final int PORT = 1099;
    
    public static void main(String[] args) {
        CalcClient client = new CalcClient();
        client.connectToCalcServer();
    }
    
    private void connectToCalcServer(){
        try{
           Registry registry = LocateRegistry.getRegistry("127.0.0.1", PORT);
           CalcInterface calculator =  (CalcInterface) registry.lookup("CalcServer");
           
           Scanner scanner = new Scanner(System.in);
            String firstNumber,
                    secondNumber;
            String operation;
            int result;

            System.out.println("Please, place first number: ");
            firstNumber = scanner.next();
            System.out.println("Please, place second number: ");
            secondNumber = scanner.next();
            System.out.println("Please, place operation: (write add or substract)");
            operation = scanner.next();
            operation = operation.toLowerCase();
            switch(operation) {
		case "add" : result = calculator.Add(firstNumber, secondNumber); break;
		case "substract" : result = calculator.Substract(firstNumber, secondNumber); break;
		default : throw new IllegalArgumentException("Opperations must be add/substract");
                        }
            
            System.out.println("result is: " + result);
           
        }
        catch(Exception exception){
            System.out.println("Catched Exception: " + exception);
        }
    }
}
