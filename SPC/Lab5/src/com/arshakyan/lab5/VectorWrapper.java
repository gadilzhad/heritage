/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arshakyan.lab5;

import java.util.LinkedList;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

/**
 *
 * @author Jerzy Arshakyan
 */
public class VectorWrapper extends LinkedList<Double> {

    public VectorWrapper(){
        super();
    }
    
    
    public void initVector(String filename) {
        Scanner scan;
        File file = new File(filename);
        try {
            scan = new Scanner(file);
            String sTemp = "";
            while (scan.hasNext()) {
                sTemp = scan.next();
                this.add(Double.parseDouble(sTemp));
            }

        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        }
    }
}
