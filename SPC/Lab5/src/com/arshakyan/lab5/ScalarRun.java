/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arshakyan.lab5;

/**
 *
 * @author Ezalor
 */
public class ScalarRun {
    
    private static VectorWrapper firstVector;
    private static VectorWrapper secondVector;

    
    public static void main(String[] args) {
        init();
        long standartTimeStart, standartTimeEnd,
             parallelTimeStart,  parallelTimeEnd;
        
        
        
        ScalarMultiplicator  scalar = new ScalarMultiplicator(firstVector, secondVector);
        standartTimeStart = System.nanoTime();
        scalar.multiple();
        standartTimeEnd = System.nanoTime();
        System.out.println(scalar.getResult());
        System.out.println(standartTimeEnd-standartTimeStart);
        
        ParallelScalarMultiplicator concurrentScalar = new ParallelScalarMultiplicator(firstVector, secondVector);
        parallelTimeStart = System.nanoTime();
        concurrentScalar.checkedMultiple();
        parallelTimeEnd = System.nanoTime();
        System.out.println(scalar.getResult());
        System.out.println(parallelTimeEnd-parallelTimeStart);
    }
    
    private static void init(){
        firstVector = new VectorWrapper();
        secondVector = new VectorWrapper();
        firstVector.initVector("firstVector.txt");
        secondVector.initVector("secondVector.txt");
    }
}
