/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arshakyan.lab5;

/**
 *
 * @author Jerzy Arshakyan
 */
public class ScalarMultiplicator {

    private Double result;
    private VectorWrapper firstVector;
    private VectorWrapper secondVector;

    ScalarMultiplicator(VectorWrapper firstVector, VectorWrapper secondVector) {
        result = 0.0;
        this.firstVector = firstVector;
        this.secondVector = secondVector;
    }

    void multiple() {
        if (isVectorsDimensionsNotEqual(firstVector, secondVector)) {
            System.out.println("Терпеть не могу исключения, а тут ещё и вектора из разных пространств перемножать -_-");
            return;
        }
        for (int i = 0; i < firstVector.size(); i++) {
            result += firstVector.get(i) * secondVector.get(i);
        }
    }
    
    Double getResult() {
        return result;
    }

    void eraseResult() {
        result = 0.0;
    }

    private boolean isVectorsDimensionsNotEqual(VectorWrapper first, VectorWrapper second) {
        if (first.size() == second.size()) {
            return false;
        } else {
            return true;
        }
    }
}
