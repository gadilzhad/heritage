/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arshakyan.lab5;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Jerzy Arshakyan
 */
public class ParallelScalarMultiplicator {

    private volatile Double result;
    private VectorWrapper firstVector;
    private VectorWrapper secondVector;

    ParallelScalarMultiplicator(VectorWrapper firstVector, VectorWrapper secondVector) {
        result = 0.0;
        this.firstVector = firstVector;
        this.secondVector = secondVector;
    }

    void multiple() throws InterruptedException {
        if (isVectorsDimensionsNotEqual(firstVector, secondVector)) {
            System.out.println("Терпеть не могу исключения, а тут ещё и вектора из разных пространств перемножать -_-");
            return;
        }
        int processors = Runtime.getRuntime().availableProcessors();
        int portion = firstVector.size() / processors;
        List<PerformerOfAddition> threads = new ArrayList();
        for (int i = 0; i < processors; ++i) {
            int from = i * portion;
            int to = i < processors - 1 ? (i + 1) * portion : firstVector.size();
            PerformerOfAddition task = new PerformerOfAddition(from, to);
            threads.add(task);
        }

        for (PerformerOfAddition t : threads) {
            t.start();
        }
        for (PerformerOfAddition t : threads) {
            t.join();
        }

    }

    void checkedMultiple() 
    {
        try {
            this.multiple();
        } catch ( InterruptedException e) {
            e.getStackTrace();
        }
    }    

        class PerformerOfAddition extends Thread {

            private int startingRowNumber;
            private int endingRowNumber;

            PerformerOfAddition(int startingRowNumber, int endingRowNumber) {
                this.startingRowNumber = startingRowNumber;
                this.endingRowNumber = endingRowNumber;
            }

            @Override
            public void run() {
                for (int i = startingRowNumber; i < endingRowNumber; i++) {
                    result += firstVector.get(i) * secondVector.get(i);
                }
            }
        }

        Double getResult
        
            () {
        return result;
        }

    

    private boolean isVectorsDimensionsNotEqual(VectorWrapper first, VectorWrapper second) {
        if (first.size() == second.size()) {
            return false;
        } else {
            return true;
        }
    }
}
