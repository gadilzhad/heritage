
package com.arshakyan.lab3;

/**
 *
 * @author Jerzy Arshakyan
 */
public class Ferrari implements CarToy {
    
    private String toyName;
    private final static String toyNoise = "WrumWruuuuuuuuuuuuum";
    private final static String markName = "Ferrari";
    
    Ferrari(){
        this.toyName = "550";
    }
    
    Ferrari(String toyName){
        this.toyName = toyName;
    }
    
    
    @Override
    public void Entertain(){
         System.out.println("Hi, I am " + markName + " " + this.toyName + ". Let's ride!");
    };
    @Override
    public void Ride(){
        System.out.println("You're riding" + this.toyName + ".");
        this.PerformNoise();
    };
    @Override
    public void PerformNoise(){
        System.out.println(markName + " doing " + toyNoise);
    };
}
