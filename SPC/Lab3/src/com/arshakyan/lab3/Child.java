
package com.arshakyan.lab3;

/**
 *
 * @author Jerzy Arshakyan
 */
public class Child {
        private String childName;
        private String childMood;
        
   Child(){
        this.childName = "Steeve";
        this.childMood = "sad"; 
    }
    
   Child(String childName){
        this.childName = childName;
        this.childMood = "sad";
    }
   
   
   public void PlayToy(Toy toy){
       this.TellMood();
       toy.Entertain();
       this.BecomeHappy();
       this.TellMood();
   };
   public void PlayCarToy(CarToy toy){
       this.TellMood();
       toy.Entertain();
       toy.Ride();
       this.BecomeHappy();
       this.TellMood();
   };
   public void PlayFerrari(Ferrari toy){
       this.TellMood();
       toy.Entertain();
       toy.Ride();
       this.BecomeHappy();
       this.TellMood();
   };
   public void PlayBarbie(Barbie toy){
       this.TellMood();
       toy.Entertain();
       this.BecomeHappy();
       this.TellMood();
   };
   
   public void BecomeBored(){
       this.childMood = "bored";
       System.out.println();
   }
   
   private void BecomeHappy(){
       this.childMood = "happy";
   }
   
   private void TellMood(){
       System.out.println("Hi, I am " + this.childName + ". I'm " + this.childMood);
   }
        
}
