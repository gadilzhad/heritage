package com.arshakyan.lab3;

/**
 *
 * @author Jerzy Arshakyan
 */
public class Barbie implements Toy {
    
    private String toyName;
    
    
    Barbie(){
        this.toyName = "Sheyla";
    }
    
    Barbie(String toyName){
        this.toyName = toyName;
    }
    
    @Override
    public void Entertain(){
        System.out.println("Hi, I am " + this.toyName + ". Let's play!");
    };
}
