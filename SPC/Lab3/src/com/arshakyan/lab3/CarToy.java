
package com.arshakyan.lab3;

/**
 *
 * @author Ezalor
 */
public interface CarToy extends Toy {
  
    
    void Ride();
    void PerformNoise();
}
