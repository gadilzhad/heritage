package com.arshakyan.lab3;

/**
 *
 * @author Ezalor
 */
public class Main {

    
    public static void main(String[] args) {
        Child valera = new Child("Valera");
        Ferrari ferrari355 = new Ferrari("355");
        Barbie doll = new Barbie("Dolly");
        
        valera.PlayFerrari(ferrari355);
        valera.BecomeBored();
        valera.PlayToy(ferrari355);
        valera.BecomeBored();
        valera.PlayCarToy(ferrari355);
        valera.BecomeBored();
        valera.PlayBarbie(doll);
        valera.BecomeBored();
        valera.PlayToy(doll);
        valera.BecomeBored();
    }
}
