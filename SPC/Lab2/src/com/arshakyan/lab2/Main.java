package com.arshakyan.lab2;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Jerzy Arshakyan
 */
public class Main {

    final static int CONTAINER_SIZE = 10000000;
    
    public static void main(String[] args) {
        ArrayList list = buildList(CONTAINER_SIZE);
        
        long arrayTimeStart, arrayTimeEnd,
             listTimeStart,  listTimeEnd;
        
        IntArray array = new IntArray(list);
        
        arrayTimeStart = System.nanoTime();
        System.out.println(linearSearch(list, 9));
        arrayTimeEnd = System.nanoTime();
        
        listTimeStart = System.nanoTime();
        System.out.println(array.linearSearch(9));
        listTimeEnd = System.nanoTime();
        
        System.out.println("On array program worked for: ");
        System.out.println(arrayTimeEnd-arrayTimeStart);
        System.out.println("On list program worked for: ");
        System.out.println(listTimeEnd-listTimeStart);
    }
    
    private static ArrayList<Integer> buildList(int size){
         Random rand = new Random();
         ArrayList<Integer> list = new ArrayList(size);
         
         for(int i = 0; i < size; i++)
         {
             list.add(i, rand.nextInt(100000000));
         }
         return list;
    }
    
    private static int linearSearch(ArrayList<Integer> list, int value)
    {
         for(int i = 0; i < list.size(); i++)
        {
            if(list.get(i) == value) {
                return i;
            }
        }
        return -1;
    }
    
    
    }

