package com.arshakyan.lab2;

import java.util.ArrayList;
/**
 *
 * @author Jerzy Arshakyan
 */
public class IntArray {

    int[] content;
    int length;

    IntArray() {
        length = 100;
        content = new int[length];
    }

    IntArray(int length) {
        this.length = length;
        content = new int[length];
    }
    
    IntArray(ArrayList<Integer> list){
       length = list.size();
       content = new int[length];
       
       for(int i = 0; i < list.size(); i++){
           content[i] = list.get(i);
       }
    }
    
    int linearSearch(int value){
        for(int i = 0; i < this.length; i++)
        {
            if(this.content[i] == value) {
                return i;
            }
        }
        return -1;
    }
}
