
import java.rmi.*;
/**
 *
 * @author Jerzy Arshakyan
 */
public interface CalcInterface extends Remote {
    int Add(String firstNumber, String secondNumber) throws RemoteException;
    int Substract(String firstNumber, String secondNumber) throws RemoteException;
}
