import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Jerzy Arshakyan
 */
public class Calc extends UnicastRemoteObject implements CalcInterface {

    private static final int PORT = 1099;
    
    public Calc() throws RemoteException {
        super();
    }
    
    @Override
    public synchronized int Add(String firstNumber, String secondNumber) throws RemoteException {
        int firstOperand = Integer.parseInt(firstNumber);
        int secondOperand = Integer.parseInt(secondNumber);
        return firstOperand + secondOperand;
    }

    @Override
    public synchronized int Substract(String firstNumber, String secondNumber) throws RemoteException {
        int firstOperand = Integer.parseInt(firstNumber);
        int secondOperand = Integer.parseInt(secondNumber);
        return firstOperand - secondOperand;
    }

    public static void main(String[] argv) throws Exception {
        try{
         Registry registry = LocateRegistry.createRegistry(PORT);
         registry.bind("CalcServer", new Calc());
         System.out.println("CalcServer has started.");
        }
        catch (Exception exception) {
            System.out.println("Catched Exception: " + exception);
        }
    }
}