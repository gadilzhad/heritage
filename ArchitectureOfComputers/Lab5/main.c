	#include <stdio.h>
	#include <stdlib.h>
	#include <windows.h>
	
	typedef struct prof_timer_t {
		LARGE_INTEGER time_start;
		LARGE_INTEGER time_stop;
	} prof_timer_t;
	
	void prof_timer_start(prof_timer_t *) ;
	void prof_timer_stop(prof_timer_t *) ;
	double prof_timer_get_duration_in_secs(prof_timer_t *) ;
	
	int main (int __argc, char** __argv ){
		const int L1 = 2 * 32 * 1024, MIN_BLOCK_SIZE = 1024, MAX_BLOCK_SIZE = 2 * L1, STEP = 1024 ;
		int c, *p = ( int*) malloc ( MAX_BLOCK_SIZE ), tmp = 0 ;
		FILE* file ;
		fopen_s ( &file,"result.txt", "w" ) ;
		prof_timer_t timer ;
		for (int b = MIN_BLOCK_SIZE; b < MAX_BLOCK_SIZE; b += STEP) {
			prof_timer_start ( &timer ) ;
			for ( c = 0; c <= b; c += sizeof(int)) {
				tmp += *(int*)((int)p + c);
				*(int*) ((int)p + c) = tmp;
			}
			prof_timer_stop ( &timer ) ;
			fprintf ( file, "%d\t%f\n", b / STEP, prof_timer_get_duration_in_secs(&timer) ) ;
		}
		fclose (file) ;
		free(p) ;
		return 0 ;
	}
	
	void prof_timer_start(prof_timer_t *timer) {
		QueryPerformanceCounter(&timer->time_start);
	}
	
	void prof_timer_stop(prof_timer_t *timer) {
		QueryPerformanceCounter(&timer->time_stop);
	}
	
	double prof_timer_get_duration_in_secs(prof_timer_t *timer) {
		LARGE_INTEGER freq;
		QueryPerformanceFrequency(&freq);
		double duration = (double)(timer->time_stop.QuadPart - timer->time_start.QuadPart);
		double scaled_duration = duration/(double)freq.QuadPart;
		return scaled_duration;
	}	
