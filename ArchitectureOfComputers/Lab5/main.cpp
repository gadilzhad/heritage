	#include <stdio.h>
	#include <stdlib.h>
	#include <windows.h>

///	  For time mesuarment:

	typedef struct TimerStructure {
		LARGE_INTEGER time_start;
		LARGE_INTEGER time_stop;
	} TimerStructure;
	
	void Start(TimerStructure *) ;
	void Stop(TimerStructure *) ;
	double GetDuration(TimerStructure *) ;
		
//////////// Few constants:
             const int L1 = 2*32*1024;
	         const int MIN_BLOCK_SIZE = 1024;
	         const int MAX_BLOCK_SIZE = 2*L1;
	         const int STEP = 1024;
	         
////////////	
	int main (){
        
        int tmp = 0;
        int *temoraryMemoryRegion = (int*) malloc (MAX_BLOCK_SIZE);
		TimerStructure timer ;
		for (int currentBlock = MIN_BLOCK_SIZE; currentBlock < MAX_BLOCK_SIZE; currentBlock += STEP) 
        {
			Start(&timer);
			for (int counter = 0; counter <= currentBlock; counter += sizeof(int)) {
				tmp += *(int*)((int)temoraryMemoryRegion + counter);
				*(int*) ((int)temoraryMemoryRegion + counter) = tmp;
			}
			Stop(&timer) ;
			printf ("%d\t%f\n", currentBlock / STEP, GetDuration(&timer) ) ;
			
		}
		//fclose (file) ;
		free(temoraryMemoryRegion) ;
        system("PAUSE");
  		return 0 ;
	}
	
	void Start(TimerStructure *timer) {
		QueryPerformanceCounter(&timer->time_start);
	}
	
	void Stop(TimerStructure *timer) {
		QueryPerformanceCounter(&timer->time_stop);
	}
	
	double GetDuration(TimerStructure *timer) {
		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);
		double duration = (double)(timer->time_stop.QuadPart - timer->time_start.QuadPart);
		double scaled_duration = duration/(double)frequency.QuadPart;
		return scaled_duration;
	}	
