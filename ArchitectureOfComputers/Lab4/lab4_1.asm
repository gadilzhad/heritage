.586
.MODEL FLAT, STDCALL
 option   casemap:none
                        include C:\masm32\INCLUDE\WINDOWS.INC
                        include C:\masm32\INCLUDE\KERNEL32.INC 
                        include C:\masm32\INCLUDE\USER32.INC
                        include C:\masm32\INCLUDE\ADVAPI32.INC                                                  
                                                                     
                        includelib C:\masm32\lib\comctl32.lib
                        includelib C:\masm32\lib\user32.lib
                        includelib C:\masm32\lib\gdi32.lib
                        includelib C:\masm32\lib\kernel32.lib                
                        includelib C:\masm32\lib\user32.lib
                        includelib C:\masm32\lib\advapi32.lib        

.data
	msgText db 256 dup(0) 
	msgFormat db "Registers: AX = %hd; BX = %hd; CX = %hd; DX = %hd", 0
	msgLabel db "Results: ", 0

	A1 DW 10 
	A2 DW 15 
	B1 DW 40
	B2 DW 25
	C1 DW 5 
	C2 DW 6
	
.code	
start: 
	xor eax, eax
	xor ebx, ebx 
	xor ecx, ecx 
	xor edx, edx 
	MOV AX, B1 
	MUL B2 
	MOV BX, AX 
	MOV AX, A1 
	SUB AX, A2 
	MOV CX, C1 
	ADD CX, C2 
	MOV DX, A1 
	SAL DX, 1 
	LOOP_START: 
		DEC CX 
		SUB BX, AX
		CMP CX, 3 
		JE LOOP_END 
		JMP LOOP_START 
		
	LOOP_END: 
		push edx 
		push ecx 
		push ebx 
		push eax 
		invoke wsprintf, addr msgText, addr msgFormat
		invoke MessageBox,    0,  addr msgText   ,addr msgLabel    ,0
EXIT:
		invoke ExitProcess, 0
end start