package com.arshakyan.lab5;

import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.Forest;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.commons.collections15.Factory;

/**
 *
 * @author Ezhy Arshakyan
 */
public class StringTree {

    private StringNode root;
    private Forest<String, Integer> jungTree;
    private int EdgeCounter = 0;
    private int vertexNumber = 0;
    
    public StringTree() {
        root = null;
    }

    public void Insert(String item) {
        if (root == null) {
            root = new StringNode(item);
        } else {
            InsertInSubTree(item);
        }
    }

    public StringNode getRoot() {
        return root;
    }
    
    public void howMuchVertexes(){
        System.out.println(vertexNumber);
    }

    private void InsertInSubTree(String item) {
        boolean isLeft = false;
        boolean isRight = false;
        StringNode current = root;
        StringNode temporaryRoot = root;

        while (temporaryRoot != null) {

            current = temporaryRoot;

            if ((item.length() - current.GetItem().length()) > 0){
                isLeft = true;
                isRight = false;
            }
            if ((item.length() - current.GetItem().length()) <= 0){
                isLeft = false;
                isRight = true;
            }
            
            if (isLeft) {
                temporaryRoot = current.GetLeftNode();
            }
            if (isRight) {
                temporaryRoot = current.GetRightNode();
            }
        }
            if (isLeft) {
                current.SetLeftNode(new StringNode(item));
            } else {
                current.SetRightNode(new StringNode(item));
            }
        }

        /*
         if (temporaryRoot.GetItem().equals(item)) {
         if(isLeft){
         temporaryRoot.SetLeftNode(new StringNode(item));
         }
         if(isRight){
         temporaryRoot.SetRightNode(new StringNode(item));    
         }
         return;
         }
         */

    

    public boolean isEmpty() {
        return (root == null);
    }

    public Forest<String, Integer> ConvertToJUNGTree() {
        jungTree = new DelegateTree<String, Integer>();
        jungTree.addVertex(root.GetItem());


        StringNode temporaryRoot = this.getRoot();

        AddEdgeToJUNGTree(temporaryRoot);

        return jungTree;
    }

    private void AddEdgeToJUNGTree(StringNode temporaryRoot) {
        Factory<Integer> edgeFactory = new Factory<Integer>() {
            public Integer create() {
                return EdgeCounter++;
            }
        };

        if (temporaryRoot != null) {

            if (temporaryRoot.GetLeftNode() != null) {
                jungTree.addEdge(edgeFactory.create(), temporaryRoot.GetItem(), temporaryRoot.GetLeftNode().GetItem());

            }
            if (temporaryRoot.GetRightNode() != null) {
                jungTree.addEdge(edgeFactory.create(), temporaryRoot.GetItem().toString(), temporaryRoot.GetRightNode().GetItem().toString());

            }

            if (temporaryRoot.GetLeftNode() != null) {
                AddEdgeToJUNGTree(temporaryRoot.GetLeftNode());
            }
            if (temporaryRoot.GetRightNode() != null) {
                AddEdgeToJUNGTree(temporaryRoot.GetRightNode());
            }
        }

    }

    public void ReadFromFile() {
        try {
            File inputFile = new File("D:/Work/ASD/Lab5/input.txt");
            BufferedReader fileReader = new BufferedReader(new FileReader(inputFile));
            StringBuilder temporaryString = new StringBuilder();

            String line = fileReader.readLine();
            while (line != null) {
                temporaryString.append(line);
                temporaryString.append("\n");
                line = fileReader.readLine();
            }

            String currentWord = "";

            for (int i = 0; i < temporaryString.length(); i++) {
                currentWord = currentWord.concat(temporaryString.substring(i, i + 1));
                if (Character.isWhitespace(temporaryString.charAt(i))) {
                    if(!StartsWith('m', currentWord)){
                    this.Insert(currentWord);    
                    }
                    currentWord = "";
                }
            }
        } catch (FileNotFoundException e) {
        } catch (IOException io) {
        }
    }
    
    public boolean StartsWith(char start, String string){
        if(string.charAt(0) == start){
            vertexNumber++;
            return true;
        }
        else
            return false;
    }
}
