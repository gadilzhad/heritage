package com.arshakyan.lab5;

import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.Forest;
import org.apache.commons.collections15.Factory;

/**
 *
 * @author Jerzy Arshakyan
 */
public class AVLTree {

    private IntegerNode root;
    private Forest<String, Integer> jungTree;
    private int EdgeCounter = 0;

    public AVLTree() {
        root = new IntegerNode();
    }

    public void Insert(Integer item) {
        Insert(item, root.GetLeftNode());
    }

    private void Insert(Integer element, IntegerNode temp) {
        if (this.root.GetLeftNode() == null) {
            this.root.SetLeftNode(new IntegerNode(element));
            return;
        }

        int compare = element.compareTo(temp.GetItem());
        if (compare <= 0) {
            if (temp.GetLeftNode() == null) {
                temp.SetLeftItem(element);
                return;
            }
            Insert(element, temp.GetLeftNode());
        } else {
            if (temp.GetRightNode() == null) {
                temp.SetRightItem(element);
                return;
            }
            Insert(element, temp.GetRightNode());

        }
        if (temp == root.GetLeftNode()) {
            rotate(root.GetLeftNode(), root);
        }
        if (temp.GetLeftNode() != null) {
            rotate(temp.GetLeftNode(), temp);
        }
        if (temp.GetRightNode() != null) {
            rotate(temp.GetRightNode(), temp);
        }
    }
    
    public IntegerNode getRoot() {
        return root.GetLeftNode();
    }

    public void rotate(IntegerNode rotateBase, IntegerNode rootAbove) {
        int balance = rotateBase.getBalance();

        IntegerNode child = (balance < 0) ? rotateBase.GetLeftNode() : rotateBase.GetRightNode();
        if (child == null) {
            return;
        }

        int childBalance = child.getBalance();
        IntegerNode grandChild = null;

        /**
         * 4 Possible variants
         *
         */
        if (balance < -1 && childBalance < 0) {
            if (rootAbove != this.root && rootAbove.GetRightNode() == rotateBase) {
                rootAbove.SetRightNode(child);
            } else {
                rootAbove.SetLeftNode(child);
            }
            grandChild = child.GetRightNode();
            child.SetRightNode(rotateBase);
            rotateBase.SetLeftNode(grandChild);
            return;
        } else if (balance > 1 && childBalance > 0) {
            if (rootAbove != this.root && rootAbove.GetRightNode() == rotateBase) {
                rootAbove.SetRightNode(child);
            } else {
                rootAbove.SetLeftNode(child);
            }

            grandChild = child.GetLeftNode();
            child.SetLeftNode(rotateBase);
            rotateBase.SetRightNode(grandChild);
            return;
        } else if (balance < -1 && childBalance > 0) {
            grandChild = child.GetRightNode();
            rotateBase.SetLeftNode(grandChild);
            child.SetRightNode(grandChild.GetLeftNode());
            grandChild.SetLeftNode(child);
            rotate(rotateBase, rootAbove);
            return;
        } else if (balance > 1 && childBalance < 0) {
            grandChild = child.GetLeftNode();
            rotateBase.SetRightNode(grandChild);
            child.SetLeftNode(grandChild.GetRightNode());
            grandChild.SetRightNode(child);
            rotate(rotateBase, rootAbove);
            return;
        }
    }
    
    public Forest<String, Integer> ConvertToJUNGTree() {
        jungTree = new DelegateTree<String, Integer>();
        jungTree.addVertex(root.GetLeftNode().GetItem().toString());

        IntegerNode temporaryRoot = this.getRoot();

        AddEdgeToJUNGTree(temporaryRoot);

        return jungTree;
    }

    private void AddEdgeToJUNGTree(IntegerNode temporaryRoot) {
        Factory<Integer> edgeFactory = new Factory<Integer>() {
            public Integer create() {
                return EdgeCounter++;
            }
        };

        if (temporaryRoot != null) {

            if (temporaryRoot.GetLeftNode() != null) {
                jungTree.addEdge(edgeFactory.create(), temporaryRoot.GetItem().toString(), temporaryRoot.GetLeftNode().GetItem().toString());

            }
            if (temporaryRoot.GetRightNode() != null) {
                jungTree.addEdge(edgeFactory.create(), temporaryRoot.GetItem().toString(), temporaryRoot.GetRightNode().GetItem().toString());

            }

            if (temporaryRoot.GetLeftNode() != null) {
                AddEdgeToJUNGTree(temporaryRoot.GetLeftNode());
            }
            if (temporaryRoot.GetRightNode() != null) {
                AddEdgeToJUNGTree(temporaryRoot.GetRightNode());
            }
        }
    }
    
    
}
