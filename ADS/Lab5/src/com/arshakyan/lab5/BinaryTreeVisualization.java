package com.arshakyan.lab5;
/*
 * 
 * @author Ezhy Arshakyan
 */


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import java.awt.GridLayout;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.BorderFactory;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ConstantTransformer;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;

import edu.uci.ics.jung.graph.Forest;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationServer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.EllipseVertexShapeTransformer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;

public class BinaryTreeVisualization extends JApplet {

    /**
     * the graph
     */
    Forest<String,Integer> jungForest;

    VisualizationViewer<String,Integer> viewer;

    VisualizationServer.Paintable rings;

    String root;

    TreeLayout<String,Integer> layout;
    @SuppressWarnings("unchecked")
	FRLayout layout1;

    @SuppressWarnings("unchecked")
	public BinaryTreeVisualization(IntegerSearchTree tree) {

        // create a simple graph for the demo
        jungForest = new DelegateForest<String,Integer>();

        jungForest = tree.ConvertToJUNGTree();

        layout = new TreeLayout<String,Integer>(jungForest);
    
        viewer =  new VisualizationViewer<String,Integer>(layout, new Dimension(600,600));
        viewer.setBackground(Color.white);
        viewer.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        viewer.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        viewer.getRenderContext().setVertexShapeTransformer(new BinaryTreeVisualization.ClusterVertexShapeFunction());
        // add a listener for ToolTips
        viewer.setVertexToolTipTransformer(new ToStringLabeller());
        viewer.getRenderContext().setArrowFillPaintTransformer(new ConstantTransformer(Color.lightGray));

        Container content = getContentPane();
        final GraphZoomScrollPane panel = new GraphZoomScrollPane(viewer);
        content.add(panel);

        final DefaultModalGraphMouse graphMouse = new DefaultModalGraphMouse();

        viewer.setGraphMouse(graphMouse);

        JComboBox modeBox = graphMouse.getModeComboBox();
        modeBox.addItemListener(graphMouse.getModeListener());
        graphMouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);

        final ScalingControl scaler = new CrossoverScalingControl();

        JButton plus = new JButton("+");
        plus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scaler.scale(viewer, 1.1f, viewer.getCenter());
            }
        });
        JButton minus = new JButton("-");
        minus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scaler.scale(viewer, 1/1.1f, viewer.getCenter());
            }
        });

     
        JPanel scaleGrid = new JPanel(new GridLayout(1,0));
        scaleGrid.setBorder(BorderFactory.createTitledBorder("Zoom"));

        JPanel controls = new JPanel();
        scaleGrid.add(plus);
        scaleGrid.add(minus);
        controls.add(scaleGrid);
        controls.add(modeBox);
        content.add(controls, BorderLayout.SOUTH);
    }


        public BinaryTreeVisualization(StringTree tree){
        // create a simple graph for the demo
        jungForest = new DelegateForest<String,Integer>();

        jungForest = tree.ConvertToJUNGTree();

        layout = new TreeLayout<String,Integer>(jungForest);
    
        viewer =  new VisualizationViewer<String,Integer>(layout, new Dimension(600,600));
        viewer.setBackground(Color.white);
        viewer.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        viewer.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        viewer.getRenderContext().setVertexShapeTransformer(new BinaryTreeVisualization.ClusterVertexShapeFunction());
        // add a listener for ToolTips
        viewer.setVertexToolTipTransformer(new ToStringLabeller());
        viewer.getRenderContext().setArrowFillPaintTransformer(new ConstantTransformer(Color.lightGray));

        Container content = getContentPane();
        final GraphZoomScrollPane panel = new GraphZoomScrollPane(viewer);
        content.add(panel);

        final DefaultModalGraphMouse graphMouse = new DefaultModalGraphMouse();

        viewer.setGraphMouse(graphMouse);

        JComboBox modeBox = graphMouse.getModeComboBox();
        modeBox.addItemListener(graphMouse.getModeListener());
        graphMouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);

        final ScalingControl scaler = new CrossoverScalingControl();

        JButton plus = new JButton("+");
        plus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scaler.scale(viewer, 1.1f, viewer.getCenter());
            }
        });
        JButton minus = new JButton("-");
        minus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scaler.scale(viewer, 1/1.1f, viewer.getCenter());
            }
        });

     
        JPanel scaleGrid = new JPanel(new GridLayout(1,0));
        scaleGrid.setBorder(BorderFactory.createTitledBorder("Zoom"));

        JPanel controls = new JPanel();
        scaleGrid.add(plus);
        scaleGrid.add(minus);
        controls.add(scaleGrid);
        controls.add(modeBox);
        content.add(controls, BorderLayout.SOUTH);   
        }
        
        
        public BinaryTreeVisualization(AVLTree tree){
        // create a simple graph for the demo
        jungForest = new DelegateForest<String,Integer>();

        jungForest = tree.ConvertToJUNGTree();

        layout = new TreeLayout<String,Integer>(jungForest);
    
        viewer =  new VisualizationViewer<String,Integer>(layout, new Dimension(600,600));
        viewer.setBackground(Color.white);
        viewer.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        viewer.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        viewer.getRenderContext().setVertexShapeTransformer(new BinaryTreeVisualization.ClusterVertexShapeFunction());
        // add a listener for ToolTips
        viewer.setVertexToolTipTransformer(new ToStringLabeller());
        viewer.getRenderContext().setArrowFillPaintTransformer(new ConstantTransformer(Color.lightGray));

        Container content = getContentPane();
        final GraphZoomScrollPane panel = new GraphZoomScrollPane(viewer);
        content.add(panel);

        final DefaultModalGraphMouse graphMouse = new DefaultModalGraphMouse();

        viewer.setGraphMouse(graphMouse);

        JComboBox modeBox = graphMouse.getModeComboBox();
        modeBox.addItemListener(graphMouse.getModeListener());
        graphMouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);

        final ScalingControl scaler = new CrossoverScalingControl();

        JButton plus = new JButton("+");
        plus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scaler.scale(viewer, 1.1f, viewer.getCenter());
            }
        });
        JButton minus = new JButton("-");
        minus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                scaler.scale(viewer, 1/1.1f, viewer.getCenter());
            }
        });

     
        JPanel scaleGrid = new JPanel(new GridLayout(1,0));
        scaleGrid.setBorder(BorderFactory.createTitledBorder("Zoom"));

        JPanel controls = new JPanel();
        scaleGrid.add(plus);
        scaleGrid.add(minus);
        controls.add(scaleGrid);
        controls.add(modeBox);
        content.add(controls, BorderLayout.SOUTH);   
        }
        



      
    class ClusterVertexShapeFunction<V> extends EllipseVertexShapeTransformer<V>
{

        ClusterVertexShapeFunction() {
            setSizeTransformer(new BinaryTreeVisualization.ClusterVertexSizeFunction<V>(20));
        }
        @SuppressWarnings("unchecked")
		@Override
        public Shape transform(V v) {
            if(v instanceof Graph) {
                int size = ((Graph)v).getVertexCount();
                if (size < 8) {   
                    int sides = Math.max(size, 3);
                    return factory.getRegularPolygon(v, sides);
                }
                else {
                    return factory.getRegularStar(v, size);
                }
            }
            return super.transform(v);
        }
    }

    /**
     * A demo class that will make vertices larger if they represent
     * a collapsed collection of original vertices
     * @author Tom Nelson
     *
     * @param <V>
     */
    class ClusterVertexSizeFunction<V> implements Transformer<V,Integer> {
    	int size;
        public ClusterVertexSizeFunction(Integer size) {
            this.size = size;
        }

        public Integer transform(V v) {
            if(v instanceof Graph) {
                return 30;
            }
            return size;
        }
    }



    public void Vizualizate(IntegerSearchTree tree) {
        JFrame frame = new JFrame();
        Container content = frame.getContentPane();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        content.add(new BinaryTreeVisualization(tree));
        frame.pack();
        frame.setVisible(true);
    }
    
    public void Vizualizate(StringTree tree) {
        JFrame frame = new JFrame();
        Container content = frame.getContentPane();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        content.add(new BinaryTreeVisualization(tree));
        frame.pack();
        frame.setVisible(true);
    }
    

     
     public void Vizualizate(AVLTree tree) {
        JFrame frame = new JFrame();
        Container content = frame.getContentPane();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        content.add(new BinaryTreeVisualization(tree));
        frame.pack();
        frame.setVisible(true);
    }
}

