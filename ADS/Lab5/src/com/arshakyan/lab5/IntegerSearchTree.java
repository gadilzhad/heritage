package com.arshakyan.lab5;

import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.Forest;
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ConstantTransformer;

/**
 *
 * @author Ezhy Arshakyan
 */
public class IntegerSearchTree {

    private IntegerNode root;
    private Forest<String, Integer> jungTree;
    private int EdgeCounter = 0;

    public IntegerSearchTree() {
        root = null;
    }

    public void Insert(Integer item) {
        if (root == null) {
            root = new IntegerNode(item);
        } else {
            InsertInSubTree(item);
        }
    }

    public Integer Find(Integer item) {
        IntegerNode temporaryRoot = root;
        int resultOfComparsion = 0;
        while (temporaryRoot != null) {
            resultOfComparsion = item - temporaryRoot.GetItem();
            if (resultOfComparsion < 0) {
                temporaryRoot = temporaryRoot.GetLeftNode();
            } else if (resultOfComparsion > 0) {
                temporaryRoot = temporaryRoot.GetRightNode();
            } else {
                return temporaryRoot.GetItem();
            }
        }
        return null;
    }

    public Integer FindMaximum() {
        return Maximum(root).GetItem();
    }

    private IntegerNode Maximum(IntegerNode node) {
        if (node.GetRightNode() == null) {
            return node;
        } else {
            return Maximum(node.GetRightNode());
        }
    }

    public IntegerNode getRoot() {
        return root;
    }

    private void InsertInSubTree(Integer item) {
        int resultOfComparsion = 0;
        IntegerNode current = root;
        IntegerNode temporaryRoot = root;

        while (temporaryRoot != null) {
            current = temporaryRoot;
            resultOfComparsion = item - current.GetItem();
            if (resultOfComparsion < 0) {
                temporaryRoot = current.GetLeftNode();
            } else if (resultOfComparsion > 0) {
                temporaryRoot = current.GetRightNode();
            } else {
                return;
            }
        }

        if (resultOfComparsion < 0) {
            current.SetLeftNode(new IntegerNode(item));
        } else {
            current.SetRightNode(new IntegerNode(item));
        }
    }

    public boolean isEmpty() {
        return (root == null);
    }

    public Forest<String, Integer> ConvertToJUNGTree() {
        jungTree = new DelegateTree<String, Integer>();
        jungTree.addVertex(root.GetItem().toString());


        IntegerNode temporaryRoot = this.getRoot();

        AddEdgeToJUNGTree(temporaryRoot);

        return jungTree;
    }

    private void AddEdgeToJUNGTree(IntegerNode temporaryRoot) {
        Factory<Integer> edgeFactory = new Factory<Integer>() {
            public Integer create() {
                return EdgeCounter++;
            }
        };

        if (temporaryRoot != null) {

            if (temporaryRoot.GetLeftNode() != null) {
                jungTree.addEdge(edgeFactory.create(), temporaryRoot.GetItem().toString(), temporaryRoot.GetLeftNode().GetItem().toString());

            }
            if (temporaryRoot.GetRightNode() != null) {
                jungTree.addEdge(edgeFactory.create(), temporaryRoot.GetItem().toString(), temporaryRoot.GetRightNode().GetItem().toString());

            }

            if (temporaryRoot.GetLeftNode() != null) {
                AddEdgeToJUNGTree(temporaryRoot.GetLeftNode());
            }
            if (temporaryRoot.GetRightNode() != null) {
                AddEdgeToJUNGTree(temporaryRoot.GetRightNode());
            }
        }
    }
}
