package com.arshakyan.lab5;


/**
 *
 * @author Ezalor
 */
public class Main {


    public static void main(String[] args) {
        
      /*
        IntegerSearchTree tree = new IntegerSearchTree();
        tree.Insert(4);
        tree.Insert(2);
        tree.Insert(5);
        tree.Insert(7);
        tree.Insert(9);
        tree.Insert(10);
        tree.Insert(8);
        tree.Insert(11);
        
       
        
        BinaryTreeVisualization BST = new BinaryTreeVisualization(tree); 
        BST.Vizualizate(tree);
    
    */
        StringTree tree = new StringTree();
        tree.ReadFromFile();
        
        BinaryTreeVisualization BST = new BinaryTreeVisualization(tree); 
        BST.Vizualizate(tree);
        
        
        /*
        AVLTree tree = new AVLTree();
        tree.Insert(4);
        tree.Insert(2);
        tree.Insert(5);
        tree.Insert(7);
        tree.Insert(9);
        tree.Insert(10);
        tree.Insert(8);
        tree.Insert(11);
 
       
        
        BinaryTreeVisualization BST = new BinaryTreeVisualization(tree); 
        BST.Vizualizate(tree);
        */
    }
    }
