/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.arshakyan.lab5;

/**
 *
 * @author Ezhy Arshakyan
 */
public class StringNode {
    
    private StringNode leftNode;
    private StringNode rightNode;
    private String item;

    public StringNode(String item) {
        this.item = item;
        leftNode = null;
        rightNode = null;
    }
    
 
    public void SetLeftNode(StringNode leftNode){
        this.leftNode = leftNode;
    }
    public void SetRightNode(StringNode rightNode){
        this.rightNode = rightNode;
    }
  
    public StringNode GetLeftNode(){
        return this.leftNode;
    }
    public StringNode GetRightNode(){
        return this.rightNode;
    }
    public String GetItem(){
        return this.item;
    }

}
