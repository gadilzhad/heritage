package com.arshakyan.lab5;

/**
 *
 * @author Ezhy Arshakyan
 */
public class IntegerNode {

    private IntegerNode leftNode;
    private IntegerNode rightNode;
    private Integer item;

    public IntegerNode(Integer item) {
        this.item = item;
        leftNode = null;
        rightNode = null;

    }

    /**
     * This four methods we need for AVLTree
     *
     */
    public IntegerNode() {
        this.item = null;
        leftNode = null;
        rightNode = null;
    }

    public void SetItem(Integer item) {
        this.item = item;
    }

    public void SetLeftItem(Integer item) {
        if (this.GetLeftNode() == null) {
            this.SetLeftNode(new IntegerNode(item));
        } else {
            this.GetLeftNode().SetItem(item);
        }
    }
    
    public void SetRightItem(Integer item) {
        if (this.GetRightNode() == null) {
            this.SetRightNode(new IntegerNode(item));
        } else {
            this.GetRightNode().SetItem(item);
        }
    }

    public void SetLeftNode(IntegerNode leftNode) {
        this.leftNode = leftNode;
    }

    public void SetRightNode(IntegerNode rightNode) {
        this.rightNode = rightNode;
    }

    public IntegerNode GetLeftNode() {
        return this.leftNode;
    }

    public IntegerNode GetRightNode() {
        return this.rightNode;
    }

    public Integer GetItem() {
        return this.item;
    }

    private int Height() {
        int leftHeight = (this.GetLeftNode() == null) ? 0 : this.GetLeftNode().Height();
        int rightHeight = (this.GetRightNode() == null) ? 0 : this.GetRightNode().Height();

        return 1 + Math.max(leftHeight, rightHeight);
    }

    public int getBalance() {
        int leftHeight = (this.GetLeftNode() == null) ? 0 : this.GetLeftNode().Height();
        int rightHeight = (this.GetRightNode() == null) ? 0 : this.GetRightNode().Height();

        return rightHeight - leftHeight;
    }
}
