package com.arshakyan.lab3;

/**
 *
 * @author Ezalor
 */
public class CharNode {

    private char character;
    private CharNode next;
    
    public CharNode(){
        character = '\0';
        next = null;
    }
    
    public CharNode(char character){
        this.character = character;
        next = null;
    }
            

    public char getCharacter(){
        return this.character;
    }
    
    public CharNode getNext(){
        return next;
    }
    public void setNext(CharNode next){
        this.next = next;
    }

}

