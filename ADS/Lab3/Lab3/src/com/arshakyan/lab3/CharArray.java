package com.arshakyan.lab3;

import java.io.*;

/**
 *
 * @author Ezhy Arshakian
 * @email G7Gadilzhad@gmail.com
 * @version 0.8
 */
public class CharArray {

    private char[] content;
    private int length;

    public CharArray() {
        length = 0;
        try {
            ReadTextFromFile();
        } catch (FileNotFoundException notFound) {
            System.out.println("File input.txt not found. Please, reload");
        } catch (IOException ioException) {
            System.out.println("File input.txt cannot be read. Please, reload");
        }
    }

    public char GetElement(int index) {
        return content[index];
    }

    public int GetLength() {
        return length;
    }

    public int GetWordsQuantity() {
        int wordsCounter = 1;

        for (int index = 0; index < length; index++) {
            if (Character.isWhitespace(content[index])) {
                wordsCounter++;
            }
        }

        return wordsCounter;
    }

    public void PrintOddWords() {
        String currentWord = "";
        for (int index = 0; index < length; index++) {
            currentWord = currentWord.concat(Character.toString(content[index]));
            if (Character.isWhitespace(content[index])) {
                PrintCurrentOddWord(currentWord);
                currentWord = "";
            }
        }
    }

    private void PrintCurrentOddWord(String currentWord) {

        if (IsOdd(currentWord.length() - 1)) {
            for (int index = 1; index < currentWord.length() - 2; index++) {
                System.out.print(currentWord.charAt(index));
            }
            System.out.println("");
        }
    }

    private boolean IsOdd(int length) {
        if ((length % 2 == 1) && (length >= 3)) {
            return true;
        } else {
            return false;
        }
    }

    private void ReadTextFromFile() throws FileNotFoundException, IOException {
        File inputFile = new File("D:/Work/ASD/Lab3/Lab3/input.txt");
        BufferedReader fileReader = new BufferedReader(new FileReader(inputFile));
        StringBuilder temporaryString = new StringBuilder();

        String line = fileReader.readLine();
        line = line.substring(1, line.length()); // GODDAMN "." at the first position!;
        while (line != null) {
            temporaryString.append(line);
            temporaryString.append("\n");
            line = fileReader.readLine();
        }

        length = temporaryString.length();
        content = new char[length];
        content = temporaryString.toString().toCharArray();

    }
}