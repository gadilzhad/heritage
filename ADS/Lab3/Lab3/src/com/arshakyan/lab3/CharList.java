package com.arshakyan.lab3;

import java.io.*;

public class CharList {

    private int length;
    private CharNode headNode;
    private CharNode currentNode;

    public CharList() {
        try {
            length = 0;
            headNode = new CharNode(ReadFileCharAt(length));
            currentNode = new CharNode(ReadFileCharAt(length + 1));
            headNode.setNext(currentNode);
            length++;
            do {
                length++;
                currentNode.setNext(AddNode(ReadFileCharAt(length)));
                currentNode = currentNode.getNext();
            } while (isNotNull(currentNode));

        } catch (FileNotFoundException notFound) {
            System.out.println("File input.txt not found. Please, reload");
        } catch (IOException ioException) {
            System.out.println("File input.txt cannot be read. Please, reload");
        }
    }

    public void PrintList() {
        currentNode = headNode;
        while (currentNode != null) {
            System.out.print(currentNode.getCharacter());
            currentNode = currentNode.getNext();
        }

    }

    private CharNode AddNode(char CharValue) {
        CharNode temporary = new CharNode(CharValue);
        if (isNotNull(temporary)) {
            return temporary;
        } else {
            return null;
        }
    }

    private boolean isNotNull(CharNode Node) {
        if (Node == null) {
            return false;
        }
        if (Node.getCharacter() != '\0') {
            return true;

        } else {
            return false;
        }
    }

    private char ReadFileCharAt(int position) throws FileNotFoundException, IOException {
        File inputFile = new File("D:/Work/ASD/Lab3/Lab3/input.txt");
        BufferedReader fileReader = new BufferedReader(new FileReader(inputFile));
        StringBuilder temporaryString = new StringBuilder();

        String line = fileReader.readLine();
        line = line.substring(1, line.length()); // GODDAMN "." at the first position!;
        while (line != null) {
            temporaryString.append(line);
            temporaryString.append("\n");
            line = fileReader.readLine();
        }
        if (position < temporaryString.length()) {
            return temporaryString.charAt(position);
        } else {
            return '\0';
        }
    }

    public void PrintOddWords() {
        String currentWord = "";
        currentNode = headNode;

        while (currentNode != null) {
            currentWord = currentWord.concat(Character.toString(currentNode.getCharacter()));
            if (Character.isWhitespace(currentNode.getCharacter())) {
                PrintWordIfOdd(currentWord);
                currentWord = "";
            }
            currentNode = currentNode.getNext();
        }
    }

    private void PrintWordIfOdd(String currentWord) {

        if (IsOdd(currentWord.length() - 1)) {
            for (int index = 1; index < currentWord.length() - 2; index++) {
                System.out.print(currentWord.charAt(index));
            }
            System.out.println("");
        }
    }

    private boolean IsOdd(int length) {
        if ((length % 2 == 1) && (length >= 3)) {
            return true;
        } else {
            return false;
        }
    }
}