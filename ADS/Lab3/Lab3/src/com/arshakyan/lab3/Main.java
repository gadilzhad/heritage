/*
 * Word - is consequence of characters, that separated with Whitespace
 * Program won't word with words like "lia.".
 * To make the PrintOddWords method work with it 
 * We need to write additional method, that will compare our character 
 * With "!"","".""?"":"... and so on;
 */
package com.arshakyan.lab3;


/**
 *
 * @author Ezalor
 */
public class Main {
    
    public static void main(String args[]) {
        
        //CharArray example = new CharArray();
        //example.PrintOddWords();
        
        CharList list = new CharList();
        list.PrintOddWords();
    }
            
}
