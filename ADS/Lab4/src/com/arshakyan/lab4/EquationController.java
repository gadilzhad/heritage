package com.arshakyan.lab4;

import java.util.Scanner;

/**
 *
 * @author Ezhy Arshakyan
 */
public class EquationController {

    static StackList<Integer> stack;

    public EquationController() {
        stack = new StackList<>();
    }

    public String GetInput() {
        PrintStartMessage();

        Scanner systemIn = new Scanner(System.in);
        String input = systemIn.nextLine();
        return input;
    }

    public void Calculate(String input) {
        char[] CharRepresentation = new char[input.length()];
        CharRepresentation = input.toCharArray();

        String currentNumber = "";
        for (int index = 0; index < input.length(); index++) {
            if (Character.isDigit(CharRepresentation[index])) {
                currentNumber = currentNumber.concat(Character.toString(CharRepresentation[index]));
            } else {
                PushInput(currentNumber);
                currentNumber = "";
                DoIfOperation(CharRepresentation[index]);
            }
        }
        if (!currentNumber.isEmpty()) {
            PushInput(currentNumber);
        }
    }

    private void PrintStartMessage() {
        System.out.println("Input equation: ");
    }

    private void PushInput(String input) {
        if (!input.isEmpty()) {
            Integer PushedValue = new Integer(input);
            stack.Push(PushedValue);
        } else {
            return;
        }
    }

    private StackList<Integer> DoIfOperation(char input) {
        switch (input) {
            case '+': {
                Integer first = stack.Pop();
                Integer second = stack.Pop();
                if (IsNotNull(first, second)) {
                    stack.Push(first + second);
                    stack.ShowStack();
                    return stack;
                }
            }
            case '-': {
                Integer first = stack.Pop();
                Integer second = stack.Pop();
                if (IsNotNull(first, second)) {
                    stack.Push(first - second);
                    stack.ShowStack();
                    return stack;
                }
            }
            case '*': {
                Integer first = stack.Pop();
                Integer second = stack.Pop();
                if (IsNotNull(first, second)) {
                    stack.Push(first * second);
                    stack.ShowStack();
                    return stack;
                }
            }
            case '/': {
                Integer first = stack.Pop();
                Integer second = stack.Pop();
                if (IsNotNull(first, second)) {
                    if (second.intValue() != 0) {
                        stack.Push(first / second);
                        stack.ShowStack();
                        return stack;
                    }
                }
            }
            case ' ': {
                return stack;
            }
            default: {
                System.err.println("Supported operations is +, -, *, /;");
                return stack;
            }
        }
    }

    private boolean IsNotNull(Integer first, Integer second) {
        if ((first != null) && (second != null)) {
            return true;
        } else {
            return false;
        }
    }
}
