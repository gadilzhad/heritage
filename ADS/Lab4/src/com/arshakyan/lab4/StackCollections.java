package com.arshakyan.lab4;

import java.util.*;

/**
 *
 * @author Ezhy Arshakyan
 */
public class StackCollections<E> extends Vector<E> {

    public StackCollections() {
    }

    public void Push(E item) {
        this.addElement(item);
    }

    public E Pop() {
        if (!isEmpty()) {
            E temporaryObject = this.Peek();
            this.removeElementAt(this.size() - 1);
            return temporaryObject;
        } else {
            return null;
        }
    }

    public E Peek() {
        if (!isEmpty()) {
            return this.elementAt(this.size() - 1);
        } else {
            return null;
        }
    }

    public boolean isEmpty() {
        return (this.size() == 0);
    }
}
