package com.arshakyan.lab4;

/**
 *
 * @author Ezhy Arshakyan
 */
public class QueueArray<E> {

    private int size;
    private E[] items;
    private int head;
    private int tail;

    public QueueArray(int size) {
        this.items = (E[]) new Object[size];
        this.size = size;
        this.tail = 0;
        this.head = 0;
    }

    public void Add(E item) {
        tail++;
        if (tail == size) {
            tail = 0;
        }
        items[tail] = item;
    }

    public E Extract() {
        head++;
        if (head == size) {
            head = 0;
        }
        return items[head];
    }

    public boolean IsEmpty() {
        if (head == size) {
            return true;
        } else {
            return false;
        }
    }
}
