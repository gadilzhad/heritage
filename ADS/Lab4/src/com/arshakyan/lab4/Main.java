package com.arshakyan.lab4;

/**
 *
 * @author Ezhy Arshakyan
 */
public class Main {

    public static void main(String[] args) {
        /*
        DoubleStackArray<Integer> doublestack = new DoubleStackArray<>(10);
        doublestack.PushForward(1);
        doublestack.PushForward(3);
        doublestack.PushForward(2);
        doublestack.PushForward(4);
        doublestack.PushBack(10);
        doublestack.PushBack(5);
        doublestack.PushBack(6);
        doublestack.PopForward();
        doublestack.ShowStack();
        */
      
        StackCollections<Integer> stack = new StackCollections<>();
        stack.Push(2);
        stack.Push(3);
        stack.Push(6);
        System.out.println(stack.Pop());
        System.out.println(stack.Peek());
        System.out.println(stack.Pop());
        
        /*
        QueueCollections queue = new QueueCollections();
        queue.Add(5);
        queue.Add(3);
        queue.Add(1);
        System.out.println(queue.Extract());
        */
     }
}
