package com.arshakyan.lab4;

/**
 *
 * @author Ezhy Arshakyan
 */
public class QueueList<E> {

    private int size;
    private Node<E> headNode;
    private Node<E> currentNode;

    public QueueList() {
        size = 0;
        headNode = new Node<E>();
        currentNode = new Node<E>();
    }

    public void Add(E item) {
        if (size >= 1) {
            currentNode.setItem(item);
            Node temporaryPrevious = new Node();
            temporaryPrevious = currentNode;
            currentNode.setNext(new Node());
            currentNode = currentNode.getNext();
            currentNode.setPrevious(temporaryPrevious);
        } else {
            headNode.setItem(item);
            headNode.setNext(currentNode);
            currentNode.setPrevious(headNode);
        }
        size++;
    }

    public E Extract() {
        if (size > 1) {
            E temporary = headNode.getItem();
            size--;
            headNode = headNode.getNext();
            return temporary;
        }
        if (size == 1) {
            E temporary = headNode.getItem();
            size--;
            headNode = new Node<E>();
            currentNode = new Node<E>();
            return temporary;
        } else {
            return null;
        }
    }

    public void ShowQueue() {
        Node<E> current = new Node();
        current = headNode;
        while (current.getNext() != null) {
            System.out.println(current.getItem());
            current = current.getNext();
        }

    }
}
