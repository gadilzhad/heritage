package com.arshakyan.lab4;

import java.util.Vector;

/**
 *
 * @author Ezhy Arshakyan
 */
public class QueueCollections<E> extends Vector<E> {

    public QueueCollections() {
    }

    public void Add(E item) {
        this.addElement(item);
    }

    public E Extract() {

        if (!isEmpty()) {
            E temporaryItem = this.firstElement();
            this.removeElementAt(0);
            return temporaryItem;
        } else {
            return null;
        }
    }


    public boolean isEmpty() {
        return (this.size() == 0);
    }
}
