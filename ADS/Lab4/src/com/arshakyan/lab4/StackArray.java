package com.arshakyan.lab4;

/**
 *
 * @author Ezhy Arshakyan
 */
public class StackArray<E> {

    private E[] items;
    private int size;
    private int capacity;

    public StackArray(int size) {
        this.capacity = size;
        this.items = (E[]) new Object[capacity];
    }

    public void Push(E item) {
        if (size <= capacity) {
            size++;
            items[size] = item;
        }
    }

    public E Pop() {
        size--;
        return items[size + 1];
    }

    public void ShowStack() {
        do {
            System.out.println(Pop());
        } while (size != 0);
    }
}
