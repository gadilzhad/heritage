
package com.arshakyan.lab4;

/**
 *
 * @author Ezhy Arshakyan
 */
public class Node<E>{
    
    private E item;
    private Node previous;
    private Node next;
    
    
    public Node(){
        this.previous = null;
        this.item = (E)new Object();
        this.next = null;
    }
    
    public void setItem(E item){
        this.item = item;
    }
    
    public E getItem(){
        return this.item;
    }
    
    public Node getNext(){
        return this.next;
    }
    
    public void setNext(Node next){
        this.next = next;
    }
    
    
    public Node getPrevious(){
        return this.previous;
    }
    
    public void setPrevious(Node previous){
        this.previous = previous;
    }
    

}
