/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.arshakyan.lab4;

/**
 *
 * @author Ezhy Arshakyan
 */
public class DoubleStackArray<E>  {

    private E[] items;
    private int headSize;
    private int tailSize;
    private int capacity;

    public DoubleStackArray(int size) {
        this.capacity = size;
        this.items = (E[]) new Object[capacity];
        this.headSize = 0;
        this.tailSize = 0;
    }

    public void PushForward(E item) {
        if(headSize <= (capacity-tailSize))
        {
            headSize++;
            items[headSize-1] = item;
        }
    }
    
    public void PushBack(E item) {
        if(tailSize <= (capacity-headSize))
        {
            tailSize++;
            items[capacity-tailSize] = item;
        }
    }

    public E PopForward() {
        headSize--;
        return items[headSize + 1];
    }
    
    public E PopBack() {
        tailSize--;
        return items[capacity - tailSize + 1];
    }

    public void ShowStack() {
        System.out.println("First Stack: ");
        for(int index = this.headSize; index > 0 ; index--){
            System.out.println(this.items[index-1]);
        }
        
        System.out.println("Second Stack: ");
        for(int index = this.tailSize; index > 0; index--){
            System.out.println(this.items[capacity - index]);
        }
    }
}