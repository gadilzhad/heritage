package com.arshakyan.lab4;

/**
 *
 * @author Ezhy Arshakyan
 */
public class StackList<E> {

    private int size;
    private Node<E> headNode;
    private Node<E> currentNode;

    public StackList() {
        size = 0;
        headNode = new Node<E>();
        currentNode = new Node<E>();
    }

    public void Push(E item) {
        if (size >= 1) {
            currentNode.setItem(item);
            Node temporaryPrevious = new Node();
            temporaryPrevious = currentNode;
            currentNode.setNext(new Node());
            currentNode = currentNode.getNext();
            currentNode.setPrevious(temporaryPrevious);
        } else {
            headNode.setItem(item);
            headNode.setNext(currentNode);
            currentNode.setPrevious(headNode);
        }
        size++;
    }

    public E Pop() {
        if (size > 1) {
            currentNode = currentNode.getPrevious();
            E temporary = currentNode.getItem();
            size--;
            return temporary;
        }
        if (size == 1)
        {
            E temporary = headNode.getItem();
            size = 0;
            headNode = new Node<E>();
            currentNode = new Node<E>();
            return temporary;
        }
        else
            return null;
        
    }
    
    public void ShowStack() {
        Node<E> current = new Node();
        current = currentNode.getPrevious();
        while (current != null){
            System.out.println(current.getItem());
            current = current.getPrevious();
        }      

    }
    
    private boolean isNotNull(Node<E> e) {
        if(e == null) return false;
        else return true;
    }
    
}
